# Go-brlapi

Bindings for brlapi in GoLang.

## `brlapi_fileDescriptor brlapi_openConnection(const brlapi_connectionSettings_t *desired, brlapi_connectionSettings_t *actual);`

`func NewConnection() error`

## `void brlapi_closeConnection(void)`

`func Close()`

## `int brlapi_enterTtyMode(int tty, const char *driver);`

- `func EnterTTYMode() error`
- `func EnterTTYModeFrom(ttyNumber int) error`

## `int brlapi_writeDots(const unsigned char *dots)`

`func WriteDots(dots []byte) error`

## `int brlapi_readKey(int wait, brlapi_keyCode_t *code);`

- `func ReadKey() (Key, error)`
- `func WaitKey() (Key, error)`

- `func GetKeysStream() KeyStream`
- `func (*KeyStream) Get() <-chan Key`
- `func (*KeyStream) Close() error`

## `int brlapi_leaveTtyMode(void);`

`func LeaveTTYMode() error`
