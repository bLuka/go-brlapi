package brlapi

// #cgo LDFLAGS: -lbrlapi
// #include <brlapi.h>
import "C"
import "unsafe"

// Key is defined as the brlapi_keyCode_t type.
type Key uint64

// KeyStream contains the type used for buffering and parsing all pressed keys.
type KeyStream struct {
	keys  <-chan Key
	close chan<- bool
	err   error
}

/*
 * GetKeyStream returns a new KeyStream.
 *
 * Don't forget to call KeyStream.Close() (you may defer it).
 */
func GetKeysStream() KeyStream {
	var (
		keys  = make(chan Key)
		close = make(chan bool, 1)
		err   error
	)

	go func() {
		for {
			select {
			case key := <-WaitKeyChan(&err):
				keys <- key
			case <-close:
				return
			}
		}
	}()

	return KeyStream{keys, close, err}
}

// Get returns the KeyStream channel. You may use it in a select or in a for loop.
func (ks *KeyStream) Get() <-chan Key {
	return ks.keys
}

// Close closes the KeyStream goroutine.
func (ks *KeyStream) Close() error {
	ks.close <- true

	return ks.err
}

// readKey asks brlapi to read a key, to block or not to block.
func readKey(blocks bool) (Key, error) {
	var (
		key     Key
		success C.int
	)

	if blocks {
		success = C.brlapi_readKey(1, (*C.brlapi_keyCode_t)(unsafe.Pointer(&key)))
	} else {
		success = C.brlapi_readKey(0, (*C.brlapi_keyCode_t)(unsafe.Pointer(&key)))
	}
	if success == -1 {
		return key, ReadFailure
	}
	return key, nil
}
