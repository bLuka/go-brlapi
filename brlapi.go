package brlapi

// #cgo LDFLAGS: -lbrlapi
// #include <brlapi.h>
import "C"
import (
	"fmt"
	"unsafe"
)

// ConnectionFailure is returned on a brlapi connection failure.
var ConnectionFailure = fmt.Errorf("Brlapi connection failed")

// EnterTTYModeFailure is returned when we cannot enter in requested TTY mode.
var EnterTTYModeFailure = fmt.Errorf("Error while entering in TTY mode")

// LeaveTTYModeFailure is returned when brlapi_leaveTtyMode() fails.
var LeaveTTYModeFailure = fmt.Errorf("Error while leaving TTY mode")

// WriteFailure is returned when we cannot write specified text to brltty.
var WriteFailure = fmt.Errorf("Error while writing dots")

// ReadFailure is returned when we cannot read specified text to brltty.
var ReadFailure = fmt.Errorf("Error while reading key")

/*
 * NewConnection tells brlapi to open a new connection. ConnectionFailure is returned on a failed connection.
 *
 * A Close() is needed before the end of the program. You may defer it.
 */
func NewConnection() error {
	success := C.brlapi_openConnection(nil, nil)
	if success == -1 {
		return ConnectionFailure
	}
	return nil
}

// Close closes the current brlapi connection.
func Close() {
	C.brlapi_closeConnection()
}

// EnterTTYMode tells brltty to take control of the default TTY. Use EnterTTYModeFrom(ttyNumber int) to ask taking control of a given TTY.
func EnterTTYMode() error {
	return EnterTTYModeFrom(int(C.BRLAPI_TTY_DEFAULT))
}

// EnterTTYModeFrom asks brltty to take control of the specified TTY.
func EnterTTYModeFrom(ttyNumber int) error {
	success := C.brlapi_enterTtyMode(C.int(ttyNumber), nil)
	if success == -1 {
		return EnterTTYModeFailure
	}
	return nil
}

// WriteDots tells brlapi to write specified dots on the screen.
func WriteDots(dots []byte) error {
	success := C.brlapi_writeDots((*C.uchar)(unsafe.Pointer(&dots[0])))
	if success == -1 {
		return WriteFailure
	}
	return nil
}

// ReadKey seeks for currently pressed keys. Its call is non-blocking.
func ReadKey() (Key, error) {
	return readKey(false)
}

// WaitKey is the blocking version of ReadKey. Returns only on error or when a key is pressed.
func WaitKey() (Key, error) {
	return readKey(true)
}

func WaitKeyChan(err *error) <-chan Key {
	var channel = make(chan Key, 1)

	go func() {
		key, _err := readKey(true)
		if _err != nil {
			if err != nil && *err != nil {
				*err = _err
			}
			close(channel)
			return
		}
		channel <- key
	}()

	return channel
}

// LeaveTTYMode asks brlapi to leave the current TTY mode.
func LeaveTTYMode() error {
	success := C.brlapi_leaveTtyMode()
	if success == 0 {
		return LeaveTTYModeFailure
	}
	return nil
}
